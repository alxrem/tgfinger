// Copyright 2017-2020 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/alxrem/goapp"
	"io/ioutil"
	"os"
)

func main() {
	os.Exit(Main())
}

func Main() int {
	var (
		telegramToken     string
		telegramTokenFile string
	)

	flag.StringVar(&telegramToken, "telegram.token", "", "Token to access Telegram API")
	flag.StringVar(&telegramTokenFile, "telegram.token-file", "", "File containing token to access Telegram API")
	flag.Parse()

	if telegramTokenFile != "" {
		if token, err := ioutil.ReadFile(telegramTokenFile); err != nil {
			log.WithError(err).WithField("file", telegramTokenFile).Error("Can't read token from file")
			return 1
		} else {
			telegramToken = string(bytes.TrimSpace(token))
		}
	} else if telegramToken == "" {
		telegramToken = os.Getenv("TELEGRAM_TOKEN")
	}

	if telegramToken == "" {
		log.Error("You must provide token to access Telegram API")
		return 1
	}

	bot, err := NewBot(telegramToken)
	if err != nil {
		log.WithError(err).Error("Can't create bot")
		return 1
	}

	app := goapp.NewApplication("tgfinger bot")
	_ = app.Start(bot)

	log.Info("Started tgfinger bot")
	app.WaitTermination()
	app.Finish()

	return 0
}
