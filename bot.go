// Copyright 2017-2020 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	log "github.com/sirupsen/logrus"
	"sync"
)

type Bot struct {
	bot     *tgbotapi.BotAPI
	updates tgbotapi.UpdatesChannel
}

func NewBot(telegramToken string) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		return nil, err
	}

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		return nil, err
	}

	return &Bot{
		bot:     bot,
		updates: updates,
	}, nil
}

func (b *Bot) Start(ctx context.Context) (*sync.WaitGroup, error) {
	var wg sync.WaitGroup

	wg.Add(1)

	go func() {
		defer wg.Done()

		for {
			select {
			case <-ctx.Done():
				return
			case update := <-b.updates:
				if update.Message == nil || update.Message.From == nil {
					continue
				}

				answer := func(message string) error {
					msg := tgbotapi.NewMessage(int64(update.Message.From.ID), message)
					_, err := b.bot.Send(msg)
					if err != nil {
						log.WithError(err).Error("Message error")
					}
					return err
				}

				var message string
				logger := log.WithFields(log.Fields{
					"ID":       update.Message.From.ID,
					"UserName": update.Message.From.UserName,
				})

				if update.Message.Chat.Type == "private" {
					if update.Message.Text == "/start" {
						logger.Info("User joined")

						if err := answer("Welcome! Forward messages here to see sender Id."); err != nil {
							return
						}
						continue
					}

					var detectedId int64
					var detectedTitle string

					if update.Message.ForwardFrom != nil {
						detected := update.Message.ForwardFrom
						detectedId = int64(detected.ID)
						detectedTitle = detected.UserName
						name := detected.FirstName + " " + detected.LastName
						message = fmt.Sprintf("%s\nId: %d\nName: %s", name, detectedId, detectedTitle)
					} else if update.Message.ForwardFromChat != nil {
						detected := update.Message.ForwardFromChat
						detectedId = detected.ID
						detectedTitle = detected.Title
						message = fmt.Sprintf("Id: %d\nTitle: %s", detectedId, detectedTitle)
					} else {
						continue
					}

					logger.WithFields(log.Fields{
						"DetectedId":    detectedId,
						"DetectedTitle": detectedTitle,
					}).Info("Detected")

					if err := answer(message); err != nil {
						return
					}
				} else if update.Message.Chat.Type == "group" || update.Message.Chat.Type == "supergroup" {
					members := update.Message.NewChatMembers
					if members == nil {
						continue
					}

					for _, member := range *members {
						if member.ID == b.bot.Self.ID {
							chat := update.Message.Chat
							message = fmt.Sprintf("Chat: %s\nChat Id: %d", chat.Title, chat.ID)
							if err := answer(message); err != nil {
								return
							}
							break
						}
					}
				}
			}
		}
	}()

	return &wg, nil
}
