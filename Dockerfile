FROM golang:1.12-alpine as builder
WORKDIR /go/src/gitlab.com/alxrem/tgfinger
COPY *.go go.mod ./
ENV GO111MODULE on
RUN apk -U add binutils git && go mod vendor && go build && strip tgfinger

FROM alpine
WORKDIR /
RUN apk -U add ca-certificates \
    && rm -f /var/cache/apk/*
COPY --from=builder /go/src/gitlab.com/alxrem/tgfinger/tgfinger .
ENTRYPOINT ["/tgfinger"]